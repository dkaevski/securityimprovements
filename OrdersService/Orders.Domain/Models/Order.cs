﻿using Common.Domain.Models;

namespace Orders.Domain.Models
{
    public class Order : OrganizationAuditableGuidEntity
    {
        public string Title { get; set; }

        public decimal Total { get; set; }
    }
}