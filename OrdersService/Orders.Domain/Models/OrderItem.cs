﻿using Common.Domain.Models;
using System;

namespace Orders.Domain.Models
{
    public class OrderItem : AuditableGuidEntity
    {
        public Guid OrganizationId { get; set; }

        public Guid OrderId { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
    }
}
