﻿using AutoMapper;
using Orders.Domain.Commands.Order.List.Models;
using Orders.Domain.Models;

namespace Orders.Domain.Mappings
{
    public class BusinessMappingProfile : Profile
    {
        public BusinessMappingProfile()
        {
            CreateMap<Order, OrderModel>();
        }
    }
}
