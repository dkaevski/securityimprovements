﻿using Common.Domain.MediatR;

namespace Orders.Domain.Commands.Report.Statistics
{
    public class StatisticsRequest : RequestBase<StatisticsResponse>
    {
    }
}
