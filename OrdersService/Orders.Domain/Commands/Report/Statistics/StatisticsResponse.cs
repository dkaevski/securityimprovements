﻿using System;

namespace Orders.Domain.Commands.Report.Statistics
{
    public class StatisticsResponse
    {
        public int OrdersCount { get; set; }

        public OrganizationStatisticsModel  Organization { get; set; }
    }

    public class OrganizationStatisticsModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Phone { get; set; }
    }
}
