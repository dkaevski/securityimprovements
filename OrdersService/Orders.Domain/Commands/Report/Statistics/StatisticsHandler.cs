﻿using AutoMapper;
using Common.Domain.Interfaces;
using Common.Domain.MediatR;
using Microsoft.EntityFrameworkCore;
using Orders.Domain.Interfaces;
using Organizations.Integration.Services.OrganizationService;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Orders.Domain.Commands.Report.Statistics
{
    public class StatisticsHandler : MediatingRequestHandler<StatisticsRequest, StatisticsResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISystemUser _systemUser;
        private readonly IOrganizationService _organizationService;

        public StatisticsHandler(IUnitOfWork unitOfWork, IMapper mapper, ISystemUser systemUser, IOrganizationService organizationService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _systemUser = systemUser;
            _organizationService = organizationService;
        }

        public override async Task<StatisticsResponse> Handle(StatisticsRequest request, CancellationToken cancellationToken)
        {
            var ordersCountTask = _unitOfWork.Orders.All().CountAsync();
            var organizationTask = _organizationService.Get(_systemUser.OrganizationId);

            await Task.WhenAll(ordersCountTask, organizationTask);

            var response = new StatisticsResponse
            {
                OrdersCount = ordersCountTask.Result,
                Organization = new OrganizationStatisticsModel
                {
                    Id = organizationTask.Result.Id,
                    Title = organizationTask.Result.Title,
                    Phone = organizationTask.Result.Phone
                }
            };

            return await Task.FromResult(response);
        }
    }
}
