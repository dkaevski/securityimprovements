﻿using Common.Domain.MediatR;

namespace Orders.Domain.Commands.Order.List
{
    public class ListRequest : RequestBase<ListResponse>
    {
    }
}
