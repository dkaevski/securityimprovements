﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Common.Domain.MediatR;
using Orders.Domain.Commands.Order.List.Models;
using Orders.Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Orders.Domain.Commands.Order.List
{
    public class ListHandler : MediatingRequestHandler<ListRequest, ListResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ListHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public override async Task<ListResponse> Handle(ListRequest request, CancellationToken cancellationToken)
        {
            List<OrderModel> orders = _unitOfWork.Orders.All()
                                                    .ProjectTo<OrderModel>(_mapper.ConfigurationProvider)
                                                    .ToList();

            return await Task.FromResult(_mapper.Map<ListResponse>(orders));
        }
    }
}
