﻿using Orders.Domain.Commands.Order.List.Models;
using System.Collections.Generic;

namespace Orders.Domain.Commands.Order.List
{
    public class ListResponse : List<OrderModel>
    { }
}
