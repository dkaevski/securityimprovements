﻿using FluentValidation;

namespace Orders.Domain.Commands.Order.List.Models
{
    public sealed class OrderModelValidator : AbstractValidator<OrderModel>
    {
        public OrderModelValidator()
        {
            RuleFor(r => r.Title).NotEmpty();
        }
    }
}
