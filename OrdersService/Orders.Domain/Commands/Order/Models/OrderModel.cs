﻿using System;

namespace Orders.Domain.Commands.Order.List.Models
{
    public class OrderModel
    {
        public Guid Id { get; set; }

        public Guid OrganizationId { get; set; }

        public string Title { get; set; }

        public decimal Total { get; set; }
    }
}
