﻿using Common.Domain.Interfaces;
using Orders.Domain.Models;

namespace Orders.Domain.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    { }
}
