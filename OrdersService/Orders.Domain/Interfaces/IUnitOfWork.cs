﻿using Common.Domain.Interfaces;

namespace Orders.Domain.Interfaces
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        IOrderRepository Orders { get; set; }
    }
}
