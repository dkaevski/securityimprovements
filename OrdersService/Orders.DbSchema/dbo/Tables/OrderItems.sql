﻿CREATE TABLE [dbo].[OrderItems] (
    [Id]             UNIQUEIDENTIFIER   NOT NULL,
    [OrganizationId] UNIQUEIDENTIFIER   NOT NULL,
    [OrderId]        UNIQUEIDENTIFIER   NOT NULL,
    [Description]    NVARCHAR (500)     NOT NULL,
    [Quantity]       INT                NOT NULL,
    [Price]          DECIMAL (18, 4)    NOT NULL,
    [CreatedBy]      UNIQUEIDENTIFIER   NOT NULL,
    [CreatedDate]    DATETIMEOFFSET (7) NOT NULL,
    [ModifiedBy]     UNIQUEIDENTIFIER   NULL,
    [ModifiedDate]   DATETIMEOFFSET (7) NULL,
    CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([Id]) ON DELETE CASCADE
);

