﻿CREATE TABLE [dbo].[Orders] (
    [Id]             UNIQUEIDENTIFIER   NOT NULL,
    [OrganizationId] UNIQUEIDENTIFIER   NOT NULL,
    [Title]          NVARCHAR (500)     NOT NULL,
    [Total]          DECIMAL (18, 4)    NOT NULL,
    [CreatedBy]      UNIQUEIDENTIFIER   NOT NULL,
    [CreatedDate]    DATETIMEOFFSET (7) NOT NULL,
    [ModifiedBy]     UNIQUEIDENTIFIER   NULL,
    [ModifiedDate]   DATETIMEOFFSET (7) NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([Id] ASC)
);

