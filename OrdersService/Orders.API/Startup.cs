using Common.Domain.Interfaces;
using Common.Domain.Middleware;
using Common.Identity.Extensions;
using Common.Identity.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Orders.Domain.Extensions;
using Orders.Infrastructure.Extensions;
using Organizations.Integration.Extensions;
using System.IO.Compression;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Orders.API
{
    public class Startup
    {
        private readonly string _assemblyTitle;

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _assemblyTitle = Assembly.GetEntryAssembly().GetName().Name;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // setup our DI
            services
                .AddSingleton<IConfiguration>(Configuration)
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .AddScoped<ISystemUser, SystemUser>();

            services.AddInfrastructureServiceCollection(Configuration);
            services.AddDomainServiceCollection();
            services.AddOrganizationIntegrationServiceCollection();
            services.AddIdentityServiceCollection();

            services.AddControllers();

            // setting jwt - identity authentication
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = Configuration.GetValue<string>("Identity:Url");
                    options.Audience = Configuration.GetValue<string>("Identity:Audience");
                    options.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = ctx =>
                        {
                            // find claim identity attached to principal.
                            var claimIdentity = (ClaimsIdentity)ctx.Principal.Identity;
                            ctx.HttpContext.User = new ClaimsPrincipal(claimIdentity);

                            // add your custom claims to the httpContext
                            ISystemUser systemUser = ctx.HttpContext.RequestServices.GetRequiredService<ISystemUser>();

                            // adding custom claims to httpContext
                            var claimsIdentity = (ClaimsIdentity)ctx.HttpContext.User.Identity;
                            claimsIdentity.AddClaims(systemUser.Claims);

                            return Task.CompletedTask;
                        }
                    };
                });

            //services.AddAuthorization(options => AuthorizationPolicies.SetPolicies(options));

            // setting gzip compression
            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });

            // adding response compression options
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMiddleware<CorrelationIdMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseResponseCompression();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync($"Hello {_assemblyTitle}");
                });
            });
        }
    }
}
