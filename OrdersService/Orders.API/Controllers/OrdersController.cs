﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Orders.Domain.Commands.Order.List;
using System.Threading.Tasks;

namespace Orders.API.Controllers
{
    public class OrdersController : MediatingController
    {
        public OrdersController(IMediator mediator) : base(mediator)
        { }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            return await HandleRequestAsync<ListRequest, ListResponse>(new ListRequest { });
        }
    }
}
