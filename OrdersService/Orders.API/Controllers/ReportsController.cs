﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Orders.Domain.Commands.Report.Statistics;
using System.Threading.Tasks;

namespace Orders.API.Controllers
{
    public class ReportsController : MediatingController
    {
        public ReportsController(IMediator mediator) : base(mediator)
        { }

        [HttpGet("statistics")]
        public async Task<IActionResult> GetStatistics()
        {
            return await HandleRequestAsync<StatisticsRequest, StatisticsResponse>(new StatisticsRequest { });
        }
    }
}
