﻿using Common.Domain.Extensions;
using Common.Domain.MediatR;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Orders.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public abstract class MediatingController : ControllerBase
    {
        protected IMediator Mediator { get; }

        protected MediatingController(IMediator mediator)
        {
            Mediator = mediator;
        }

        protected async Task<IActionResult> HandleRequestAsync<TRequest, TResponse>(TRequest request)
            where TRequest : RequestBase<TResponse>
            where TResponse : class
        {
            if (request == null)
            {
                return BadRequest();
            }

            request.CorrelationId = HttpContext.Request.Headers.GetCorrelationIdFromHeader();

            var response = await Mediator.Send(request);

            return Ok(response);
        }
    }
}
