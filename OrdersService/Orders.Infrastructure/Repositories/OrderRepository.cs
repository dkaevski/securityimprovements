﻿using Common.Domain.Interfaces;
using Common.Infrastructure.Repositories;
using Orders.Domain.Interfaces;
using Orders.Domain.Models;

namespace Orders.Infrastructure.Repositories
{
    public class OrderRepository : OrganizationBaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(ISystemUser systemUser, DatabaseContext context) : base(systemUser, context)
        { }
    }
}
