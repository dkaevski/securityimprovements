﻿using Common.Domain.Interfaces;
using Common.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Orders.Domain.Models;

namespace Orders.Infrastructure
{
    public class DatabaseContext : BaseDatabaseContext
    {
        public DatabaseContext(ISystemUser systemUser, DbContextOptions options) : base(systemUser, options)
        { }

        public DbSet<Order> Orders { get; set; }
    }
}
