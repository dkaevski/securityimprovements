﻿using Common.Infrastructure;
using Orders.Domain.Interfaces;

namespace Orders.Infrastructure
{
    public class UnitOfWork : BaseUnitOfWork, IUnitOfWork
    {
        public IOrderRepository Orders { get; set; }

        public UnitOfWork(
            DatabaseContext dbContext,
            IOrderRepository orderRepository) : base(dbContext)
        {
            Orders = orderRepository;
        }
    }
}
