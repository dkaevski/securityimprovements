﻿using MediatR;
using Newtonsoft.Json;
using System;

namespace Common.Domain.MediatR
{
    public abstract class RequestBase<TResponse> : IRequest<TResponse> where TResponse : class
    {
        [JsonIgnore]
        public Guid CorrelationId { get; set; }
    }
}
