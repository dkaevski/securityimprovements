﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.Domain.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Take first characters
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string TakeFirst(this string text, int length)
        {
            return new String(text.Take(length).ToArray());
        }

        /// <summary>
        /// Remove HTML Tags
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripHTML(this string text)
        {
            return Regex.Replace(Regex.Replace(text, @"<.*?>", String.Empty), @"\s+", " ");
        }

        /// <summary>
        /// Generate GUID from string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static Guid ToGuid(this string text)
        {
            using MD5 md5 = MD5.Create();
            byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(text));
            return new Guid(hash);
        }
    }
}
