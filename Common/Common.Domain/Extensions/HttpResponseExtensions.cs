﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Common.Domain.Extensions
{
    public static class HttpResponseExtensions
    {
        /// <summary>
        /// Returns the deserialized result if the IsSuccessStatusCode property for the HTTP response is true. 
        /// Throws an exception if the IsSuccessStatusCode property for the HTTP response is false.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        public static async Task<TResult> GetResultAsync<TResult>(this HttpResponseMessage response)
        {
            // Throws an exception if the IsSuccessStatusCode property for the HTTP response is false.
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResult>(result);
        }
    }
}
