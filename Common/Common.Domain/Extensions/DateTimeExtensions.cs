﻿using System;
using System.Data.SqlTypes;

namespace Common.Domain.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Return if the DateTime is valid SQL DateTime
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool IsValidSQLDateTime(this DateTime? dateTime)
        {
            if (dateTime == null) return true;

            DateTime minValue = (DateTime)SqlDateTime.MinValue;
            DateTime maxValue = (DateTime)SqlDateTime.MaxValue;

            return (dateTime.Value >= minValue && dateTime.Value <= maxValue);
        }

        /// <summary>
        /// Converts DateTimeOffset to given time zone
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public static DateTimeOffset ToTimeZone(this DateTimeOffset dateTime, string timeZoneId)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            return TimeZoneInfo.ConvertTime(dateTime, timeZone);
        }
    }
}
