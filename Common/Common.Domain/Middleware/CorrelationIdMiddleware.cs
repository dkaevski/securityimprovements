﻿using Common.Domain.Extensions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Common.Domain.Middleware
{
    /// <summary>
    /// Ensure that Correlation Id Header exists in the Request Header and pass it to the Response Header
    /// </summary>
    public class CorrelationIdMiddleware
    {
        private readonly RequestDelegate _next;

        public CorrelationIdMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            httpContext.Request.Headers.EnsureCorrelationIdHeader();

            var correlationId = httpContext.Request.Headers
                                                   .GetCorrelationIdFromHeader()
                                                   .ToString();

            httpContext.Response.Headers.Add(Constants.HttpHeaderConstants.CorrelationIdHeaderKey, correlationId);

            // Let the delegate execute down the middleware pipeline
            await _next(httpContext);
        }
    }
}
