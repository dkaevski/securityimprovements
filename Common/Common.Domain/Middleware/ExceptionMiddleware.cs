﻿using Common.Domain.Errors;
using Common.Domain.Exceptions;
using Common.Domain.Extensions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Common.Domain.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(
            RequestDelegate next
            )
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                // Let the delegate execute down the middleware pipeline
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);

                LogException(httpContext, ex);
            }
        }

        private void LogException(HttpContext httpContext, Exception ex)
        {
            Guid correlationId = httpContext.Request.Headers.GetCorrelationIdFromHeader();

            // log your exception here
        }

        private Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;

            var errorResponse = new ErrorDetails
            {
                CorrelationId = httpContext.Request.Headers.GetCorrelationIdFromHeader()
            };

            if (exception.GetType() == typeof(BusinessException))
            {
                var bException = (BusinessException)exception;

                errorResponse.Code = bException.Code;
                if (bException.HttpStatusCode.HasValue)
                {
                    httpStatusCode = bException.HttpStatusCode.Value;
                }
            }
            else if (exception.GetType() == typeof(ValidationException))
            {
                var vException = (ValidationException)exception;

                errorResponse.Messages = vException.Errors
                                                   .Select(e => new ErrorMessage { Message = e.ErrorMessage, ErrorCode = e.ErrorCode })
                                                   .ToList();
            }
            else
            {
                var message = !string.IsNullOrEmpty(exception.Message) ? exception.Message : "Internal Server Error";
                errorResponse.Messages = new List<ErrorMessage> { new ErrorMessage(message) };
            }

            errorResponse.InnerMessage = exception.InnerException?.Message;

            httpContext.Response.StatusCode = (int)httpStatusCode;
            httpContext.Response.ContentType = "application/json";
            return httpContext.Response.WriteAsync(errorResponse.SerializeObject());
        }
    }
}
