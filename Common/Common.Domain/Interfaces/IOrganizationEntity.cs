﻿using System;

namespace Common.Domain.Interfaces
{
    public interface IOrganizationEntity
    {
        Guid OrganizationId { get; set; }
    }
}
