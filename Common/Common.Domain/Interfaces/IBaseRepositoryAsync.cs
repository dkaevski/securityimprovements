﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Common.Domain.Interfaces
{
    public interface IBaseRepositoryAsync<TEntity> where TEntity : class
    {
        IQueryable<TEntity> All();

        Task<TEntity> GetByIdAsync(long id);

        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> query);
        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> query);

        Task<List<TEntity>> ToListAsync();
        Task<List<TEntity>> ToListAsync(Func<IQueryable<TEntity>, IQueryable<TEntity>> query);

        IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate);

        Task AddAsync(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);

        Task UpdateAsync(TEntity entity);

        Task RemoveAsync(long id);
        Task RemoveAsync(TEntity entity);
    }
}
