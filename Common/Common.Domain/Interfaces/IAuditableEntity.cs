﻿using System;

namespace Common.Domain.Interfaces
{
    public interface IAuditableEntity
    {
        Guid CreatedBy { get; set; }

        DateTimeOffset CreatedDate { get; set; }

        Guid? ModifiedBy { get; set; }

        DateTimeOffset? ModifiedDate { get; set; }
    }
}
