﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;

namespace Common.Domain.Interfaces
{
    public interface ISystemUser : IIdentity
    {
        Guid UserId { get; }
        Guid OrganizationId { get; }

        List<Claim> Claims { get; }
    }
}
