﻿using Common.Domain.Interfaces;

namespace Common.Domain.Models
{
    public abstract class BaseEntity : IEntity
    { }

    public abstract class BaseEntity<T> : IEntity<T>
    {
        public T Id { get; set; }
    }
}
