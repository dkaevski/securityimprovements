﻿using System;

namespace Common.Domain.Models
{
    public abstract class BaseGuidEntity : BaseEntity<Guid>
    {
        public BaseGuidEntity()
        {
            GenerateNewIdentity();
        }

        public bool IsTransient()
        {
            return Id == Guid.Empty;
        }

        public void GenerateNewIdentity()
        {
            if (IsTransient())
            {
                Id = Guid.NewGuid();
            }
        }

        public void ChangeCurrentIdentity(Guid identity)
        {
            if (identity != Guid.Empty)
                Id = identity;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is BaseGuidEntity))
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            BaseGuidEntity item = (BaseGuidEntity)obj;

            if (item.IsTransient() || IsTransient())
                return false;
            else
                return item.Id == Id;
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                return Id.GetHashCode();
            }
            else
            {
                return base.GetHashCode();
            }
        }

        public static bool operator ==(BaseGuidEntity left, BaseGuidEntity right)
        {
            if (Object.Equals(left, null))
                return (Object.Equals(right, null)) ? true : false;
            else
                return left.Equals(right);
        }

        public static bool operator !=(BaseGuidEntity left, BaseGuidEntity right)
        {
            return !(left == right);
        }
    }
}
