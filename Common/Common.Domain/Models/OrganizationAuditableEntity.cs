﻿using Common.Domain.Interfaces;
using System;

namespace Common.Domain.Models
{
    public class OrganizationAuditableEntity : AuditableEntity, IOrganizationEntity
    {
        public Guid OrganizationId { get; set; }
    }
}
