﻿using Common.Domain.Interfaces;
using System;

namespace Common.Domain.Models
{
    public class OrganizationAuditableGuidEntity : AuditableGuidEntity, IOrganizationEntity
    {
        public Guid OrganizationId { get; set; }
    }
}
