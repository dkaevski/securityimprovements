﻿using Common.Domain.Interfaces;
using System;

namespace Common.Domain.Models
{
    public abstract class AuditableEntity : BaseEntity, IAuditableEntity
    {
        public Guid CreatedBy { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public Guid? ModifiedBy { get; set; }

        public DateTimeOffset? ModifiedDate { get; set; }
    }
}
