﻿namespace Common.Domain.Constants
{
    public static class HttpHeaderConstants
    {
        public const string CorrelationIdHeaderKey = "X-CorrelationId";
        public const string AuthorizationHeaderKey = "Authorization";
    }
}
