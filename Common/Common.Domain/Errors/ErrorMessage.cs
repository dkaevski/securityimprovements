﻿namespace Common.Domain.Errors
{
    public sealed class ErrorMessage
    {
        public ErrorMessage() { }

        public ErrorMessage(string message)
        {
            Message = message;
        }

        public string Message { get; set; }

        public string ErrorCode { get; set; }
    }
}
