﻿using System;
using System.Collections.Generic;

namespace Common.Domain.Errors
{
    public sealed class ErrorDetails
    {
        public ErrorDetails() { }

        public ErrorDetails(int? code, string message)
        {
            Code = code;
            Messages = new List<ErrorMessage> { new ErrorMessage(message) };
        }

        public int? Code { get; set; }

        public Guid CorrelationId { get; set; }

        public List<ErrorMessage> Messages { get; set; }

        public string InnerMessage { get; set; }
    }
}
