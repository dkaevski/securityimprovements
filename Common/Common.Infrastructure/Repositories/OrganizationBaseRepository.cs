﻿using Common.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Common.Infrastructure.Repositories
{
    public abstract class OrganizationBaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IOrganizationEntity
    {
        protected ISystemUser SystemUser { get; set; }

        protected readonly DbContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        protected OrganizationBaseRepository(ISystemUser systemUser, DbContext dbContext)
        {
            SystemUser = systemUser;
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            DbSet = DbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> All()
        {
            return DbSet.Where(x => x.OrganizationId.Equals(SystemUser.OrganizationId));
        }

        public virtual TEntity GetById(Guid id)
        {
            var entity = DbSet.Find(id);

            CheckEntityAuthorization(entity);

            return entity;
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.FirstOrDefault(predicate);
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> query)
        {
            return query(All()).FirstOrDefault(predicate);
        }

        public virtual TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> query)
        {
            return query(All()).SingleOrDefault(predicate);
        }

        public virtual List<TEntity> ToList()
        {
            return All().ToList();
        }

        public virtual List<TEntity> ToList(Func<IQueryable<TEntity>, IQueryable<TEntity>> query)
        {
            return query(All()).ToList();
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return All().Where(predicate);
        }

        public virtual void Add(TEntity entity)
        {
            CheckEntityAuthorization(entity);

            DbSet.Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Add(entity);
            }
        }

        public virtual void Update(TEntity entity)
        {
            CheckEntityAuthorization(entity);

            DbSet.Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Remove(Guid id)
        {
            var entity = GetById(id);
            if (entity == null)
            {
                return;
            }

            Remove(entity);
        }

        public virtual void Remove(TEntity entity)
        {
            CheckEntityAuthorization(entity);

            DbSet.Remove(entity);
        }

        protected void CheckEntityAuthorization(TEntity entity)
        {
            if (entity != null
                && !entity.OrganizationId.Equals(Guid.Empty)
                && !entity.OrganizationId.Equals(SystemUser.OrganizationId))
            {
                throw new UnauthorizedAccessException("Can't access this entity");
            }
        }
    }
}
