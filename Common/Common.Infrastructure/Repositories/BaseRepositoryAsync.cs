﻿using Common.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Common.Infrastructure.Repositories
{
    public abstract class BaseRepositoryAsync<TEntity> : IBaseRepositoryAsync<TEntity> where TEntity : class
    {
        protected readonly DbContext DbContext;
        protected readonly DbSet<TEntity> DbSet;

        protected BaseRepositoryAsync(DbContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            DbSet = DbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> All()
        {
            return DbSet;
        }

        public virtual async Task<TEntity> GetByIdAsync(long id)
        {
            return await DbSet.FindAsync(id);
        }

        public virtual async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await DbSet.FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> query)
        {
            return await query(DbSet).FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IQueryable<TEntity>> query)
        {
            return await query(DbSet).SingleOrDefaultAsync(predicate);
        }

        public virtual async Task<List<TEntity>> ToListAsync()
        {
            return await DbSet.ToListAsync();
        }

        public virtual async Task<List<TEntity>> ToListAsync(Func<IQueryable<TEntity>, IQueryable<TEntity>> query)
        {
            return await query(DbSet).ToListAsync();
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public virtual async Task AddAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);
        }

        public virtual async Task UpdateAsync(TEntity entity)
        {
            await Task.FromResult(DbSet.Attach(entity));
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual async Task RemoveAsync(long id)
        {
            var entity = await GetByIdAsync(id);
            if (entity == null)
            {
                return;
            }

            await RemoveAsync(entity);
        }

        public virtual async Task RemoveAsync(TEntity entity)
        {
            await Task.FromResult(DbSet.Remove(entity));
        }
    }
}
