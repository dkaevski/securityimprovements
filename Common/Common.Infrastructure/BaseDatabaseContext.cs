﻿using Common.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Common.Infrastructure
{
    public class BaseDatabaseContext : DbContext
    {
        private readonly ISystemUser _systemUser;

        public BaseDatabaseContext(ISystemUser systemUser, DbContextOptions options) : base(options)
        {
            _systemUser = systemUser;
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        protected virtual void OnBeforeSaving()
        {
            var utcNow = DateTime.UtcNow;

            var addedAuditableEntities = ChangeTracker.Entries<IAuditableEntity>().Where(e => e.State == EntityState.Added).ToList();
            foreach (var entity in addedAuditableEntities)
            {
                entity.Entity.CreatedBy = _systemUser.UserId;
                entity.Entity.CreatedDate = utcNow;
            }

            var modifiedAuditableEntities = ChangeTracker.Entries<IAuditableEntity>().Where(e => e.State == EntityState.Modified).ToList();
            foreach (var entity in modifiedAuditableEntities)
            {
                entity.Entity.ModifiedBy = _systemUser.UserId;
                entity.Entity.ModifiedDate = utcNow;
            }

            var addedOrganizationEntities = ChangeTracker.Entries<IOrganizationEntity>().Where(e => e.State == EntityState.Added).ToList();
            foreach (var entity in addedOrganizationEntities)
            {
                entity.Entity.OrganizationId = _systemUser.OrganizationId;
            }
        }
    }
}
