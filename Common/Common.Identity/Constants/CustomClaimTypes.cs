﻿namespace Common.Identity.Constants
{
    public class CustomClaimTypes
    {
        public const string Role = "demo/role";
        public const string Organization = "demo/organization";
    }
}
