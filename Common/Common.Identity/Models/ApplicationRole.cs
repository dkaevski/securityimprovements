﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Common.Identity.Models
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        [Required]
        public Guid OrganizationId { get; set; }

        [Required, MaxLength(256)]
        public string Title { get; set; }

        public ApplicationRole() { }

        public ApplicationRole(string title, Guid organizationId) : base($"{organizationId}_{title}".ToLower())
        {
            Title = title;
            OrganizationId = organizationId;
        }

        public void UpdateTitle(string title)
        {
            Name = $"{OrganizationId}_{title}".ToLower();
            Title = title;
        }
    }
}
