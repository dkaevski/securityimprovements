﻿using Common.Domain.Interfaces;
using Common.Identity.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Common.Identity.Models
{
    public class SystemUser : ISystemUser
    {
        public bool IsAuthenticated { get; protected set; }
        public string AuthenticationType { get; protected set; }
        public Guid UserId { get; protected set; }
        public Guid OrganizationId { get; protected set; }

        public string Name { get; protected set; }

        public List<Claim> Claims { get; protected set; } = new List<Claim>();

        public SystemUser(IHttpContextAccessor contextAccessor, IConfiguration configuration)
        {
            IsAuthenticated = contextAccessor.HttpContext.User.Identity.IsAuthenticated;
            AuthenticationType = contextAccessor.HttpContext.User.Identity.AuthenticationType;

            var nameIdentifier = contextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var organizationIdentifier = contextAccessor.HttpContext.User.FindFirst(CustomClaimTypes.Organization)?.Value;

            UserId = string.IsNullOrEmpty(nameIdentifier) ? Guid.Empty : new Guid(nameIdentifier);
            OrganizationId = string.IsNullOrEmpty(organizationIdentifier) ? Guid.Empty : new Guid(organizationIdentifier);

            Name = nameIdentifier;
        }
    }
}
