﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Common.Identity.Models
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public Guid? OrganizationId { get; set; }

        public Guid? RoleId { get; set; }
    }
}
