﻿using IdentityModel.Client;

namespace Common.Identity.Services.DiscoveryDocument
{
    public interface IDiscoveryDocument
    {
        DiscoveryDocumentResponse Document { get; }
    }
}
