﻿using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Common.Identity.Services.DiscoveryDocument
{
    public class DiscoveryDocument : IDiscoveryDocument
    {
        private readonly string _identityUrl;

        public DiscoveryDocumentResponse Document { get; }

        public DiscoveryDocument(IConfiguration configuration)
        {
            _identityUrl = configuration.GetSection("Identity:Url").Value;

            Document = GetDiscoveryDocument().Result;
        }

        private async Task<DiscoveryDocumentResponse> GetDiscoveryDocument()
        {
            using HttpClient client = new HttpClient();
            var discoveryDocument = await client.GetDiscoveryDocumentAsync(_identityUrl);
            if (discoveryDocument.IsError)
            {
                throw new Exception($"DiscoveryDocument failed.", discoveryDocument.Exception);
            }

            return discoveryDocument;
        }
    }

}
