﻿using Common.Identity.Services.DiscoveryDocument;
using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Common.Identity.Services.ClientService
{
    public class ClientService : IClientService
    {
        private readonly IConfigurationSection _identitySection;
        private readonly IDiscoveryDocument _discoveryDocument;

        public ClientService(IConfiguration configuration, IDiscoveryDocument discoveryDocument)
        {
            _identitySection = configuration.GetSection("Identity");
            _discoveryDocument = discoveryDocument;
        }

        public async Task<TokenResponse> RequestClientCredentialsToken()
        {
            using HttpClient client = new HttpClient();
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = _discoveryDocument.Document.TokenEndpoint,

                ClientId = _identitySection.GetValue<string>("ClientId"),
                ClientSecret = _identitySection.GetValue<string>("ClientSecret"),
                Scope = _identitySection.GetValue<string>("Scope"),
            });

            if (tokenResponse.IsError)
            {
                throw new Exception($"RequestClientCredentialsToken failed.", tokenResponse.Exception);
            }

            return tokenResponse;
        }

        public async Task<TokenResponse> RequestPasswordToken(string userName, string password)
        {
            using HttpClient client = new HttpClient();
            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = _discoveryDocument.Document.TokenEndpoint,

                ClientId = _identitySection.GetValue<string>("ROPIdentity:ClientId"),
                ClientSecret = _identitySection.GetValue<string>("ROPIdentity:ClientSecret"),
                Scope = _identitySection.GetValue<string>("ROPIdentity:Scope"),

                UserName = userName,
                Password = password
            });

            if (tokenResponse.IsError)
            {
                throw new Exception($"RequestPasswordToken failed.", tokenResponse.Exception);
            }

            return tokenResponse;
        }

        public async Task<TokenResponse> RequestRefreshToken(string refreshToken)
        {
            using HttpClient client = new HttpClient();

            var tokenResponse = await client.RequestRefreshTokenAsync(new RefreshTokenRequest
            {
                Address = _discoveryDocument.Document.TokenEndpoint,

                ClientId = _identitySection.GetValue<string>("ROPIdentity:ClientId"),
                ClientSecret = _identitySection.GetValue<string>("ROPIdentity:ClientSecret"),

                RefreshToken = refreshToken
            });

            if (tokenResponse.IsError)
            {
                throw new Exception($"RefreshToken failed.", tokenResponse.Exception);
            }

            return tokenResponse;
        }
    }
}
