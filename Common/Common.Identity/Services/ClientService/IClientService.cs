﻿using IdentityModel.Client;
using System.Threading.Tasks;

namespace Common.Identity.Services.ClientService
{
    public interface IClientService
    {
        Task<TokenResponse> RequestClientCredentialsToken();

        Task<TokenResponse> RequestPasswordToken(string userName, string password);

        Task<TokenResponse> RequestRefreshToken(string refreshToken);
    }
}
