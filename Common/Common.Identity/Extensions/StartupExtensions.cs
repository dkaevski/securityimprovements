﻿using Common.Identity.Services.ClientService;
using Common.Identity.Services.DiscoveryDocument;
using Microsoft.Extensions.DependencyInjection;

namespace Common.Identity.Extensions
{
    public static class StartupExtensions
    {
        public static void AddIdentityServiceCollection(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IDiscoveryDocument, DiscoveryDocument>()
                             .AddScoped<IClientService, ClientService>();
        }
    }
}
