﻿using Common.Domain.Interfaces;
using Organizations.Domain.Models;

namespace Organizations.Domain.Interfaces
{
    public interface IOrganizationRepository : IBaseRepository<Organization>
    { }
}
