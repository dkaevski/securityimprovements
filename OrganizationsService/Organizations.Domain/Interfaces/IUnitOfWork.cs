﻿using Common.Domain.Interfaces;

namespace Organizations.Domain.Interfaces
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        IOrganizationRepository Organizations { get; set; }
    }
}
