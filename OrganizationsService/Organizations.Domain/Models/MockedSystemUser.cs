﻿using Common.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Common.Domain.Models
{
    public class MockedSystemUser : ISystemUser
    {
        public bool IsAuthenticated => true;
        public string AuthenticationType => "Token";
        public Guid UserId => Guid.Empty;
        public Guid OrganizationId => Guid.Empty;

        public string Name => "System";

        public List<Claim> Claims { get; protected set; } = new List<Claim>();
    }
}
