﻿using Common.Domain.Models;

namespace Organizations.Domain.Models
{
    public class Organization : AuditableGuidEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }
    }
}