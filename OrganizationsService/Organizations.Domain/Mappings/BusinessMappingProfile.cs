﻿using AutoMapper;
using Organizations.Domain.Commands.Organization.Get;
using Organizations.Domain.Commands.Organization.Models;
using Organizations.Domain.Models;

namespace Organizations.Domain.Mappings
{
    public class BusinessMappingProfile : Profile
    {
        public BusinessMappingProfile()
        {
            CreateMap<Organization, OrganizationModel>();
            CreateMap<Organization, GetResponse>();
        }
    }
}
