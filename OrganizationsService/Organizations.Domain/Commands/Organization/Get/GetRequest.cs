﻿using Common.Domain.MediatR;
using System;

namespace Organizations.Domain.Commands.Organization.Get
{
    public class GetRequest : RequestBase<GetResponse>
    {
        public Guid Id { get; set; }
    }
}
