﻿using AutoMapper;
using Common.Domain.MediatR;
using Organizations.Domain.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Organizations.Domain.Commands.Organization.Get
{
    public class GetHandler : MediatingRequestHandler<GetRequest, GetResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public override async Task<GetResponse> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            var dbOrganization = _unitOfWork.Organizations.GetById(request.Id);

            return await Task.FromResult(_mapper.Map<GetResponse>(dbOrganization));
        }
    }
}
