﻿using FluentValidation;

namespace Organizations.Domain.Commands.Organization.Get
{
    public sealed class GetRequestValidator : AbstractValidator<GetRequest>
    {
        public GetRequestValidator()
        {
            RuleFor(r => r.Id).NotEmpty();
        }
    }
}
