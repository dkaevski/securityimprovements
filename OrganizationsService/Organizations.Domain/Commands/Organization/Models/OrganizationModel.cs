﻿using System;

namespace Organizations.Domain.Commands.Organization.Models
{
    public class OrganizationModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }
    }
}
