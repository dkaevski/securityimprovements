﻿CREATE TABLE [dbo].[Organizations] (
    [Id]           UNIQUEIDENTIFIER   NOT NULL,
    [Title]        NVARCHAR (50)      NOT NULL,
    [Description]  NVARCHAR (200)     NULL,
    [Phone]        NVARCHAR (20)      NULL,
    [Website]      NVARCHAR (250)     NULL,
    [CreatedBy]    UNIQUEIDENTIFIER   NOT NULL,
    [CreatedDate]  DATETIMEOFFSET (7) NOT NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER   NULL,
    [ModifiedDate] DATETIMEOFFSET (7) NULL,
    CONSTRAINT [PK_Organizations] PRIMARY KEY CLUSTERED ([Id] ASC)
);

