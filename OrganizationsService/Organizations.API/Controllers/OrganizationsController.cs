﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Organizations.Domain.Commands.Organization.Get;
using System;
using System.Threading.Tasks;

namespace Organizations.API.Controllers
{
    public class OrganizationsController : MediatingController
    {
        public OrganizationsController(IMediator mediator) : base(mediator)
        { }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await HandleRequestAsync<GetRequest, GetResponse>(new GetRequest { Id = id });
        }
    }
}
