﻿using Common.Infrastructure;
using Organizations.Domain.Interfaces;

namespace Organizations.Infrastructure
{
    public class UnitOfWork : BaseUnitOfWork, IUnitOfWork
    {
        public IOrganizationRepository Organizations { get; set; }

        public UnitOfWork(
            DatabaseContext dbContext,
            IOrganizationRepository organizationRepository) : base(dbContext)
        {
            Organizations = organizationRepository;
        }
    }
}
