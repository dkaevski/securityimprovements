﻿using Common.Infrastructure.Repositories;
using Organizations.Domain.Interfaces;
using Organizations.Domain.Models;

namespace Organizations.Infrastructure.Repositories
{
    public class OrganizationRepository : BaseRepository<Organization>, IOrganizationRepository
    {
        public OrganizationRepository(DatabaseContext context) : base(context)
        { }
    }
}
