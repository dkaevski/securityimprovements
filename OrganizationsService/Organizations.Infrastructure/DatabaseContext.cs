﻿using Common.Domain.Interfaces;
using Common.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Organizations.Domain.Models;

namespace Organizations.Infrastructure
{
    public class DatabaseContext : BaseDatabaseContext
    {
        public DatabaseContext(ISystemUser systemUser, DbContextOptions options) : base(systemUser, options)
        { }

        public DbSet<Organization> Organizations { get; set; }
    }
}
