﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Organizations.Domain.Interfaces;
using Organizations.Infrastructure.Repositories;

namespace Organizations.Infrastructure.Extensions
{
    public static class StartupExtensions
    {
        public static void AddInfrastructureServiceCollection(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(configuration.GetValue<string>("ConnectionString")));

            serviceCollection
                .AddTransient<IUnitOfWork, UnitOfWork>()

                // repositories
                .AddScoped<IOrganizationRepository, OrganizationRepository>();
        }
    }
}
