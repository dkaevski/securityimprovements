﻿using Organizations.Domain.Commands.Organization.Get;
using System;
using System.Threading.Tasks;

namespace Organizations.Integration.Services.OrganizationService
{
    public interface IOrganizationService
    {
        public Task<GetResponse> Get(Guid id);
    }
}
