﻿using Common.Domain.Extensions;
using Common.Identity.Services.ClientService;
using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using Organizations.Domain.Commands.Organization.Get;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Organizations.Integration.Services.OrganizationService
{
    public class OrganizationService : IOrganizationService
    {
        private readonly IClientService _clientService;
        private readonly string _organizationsAPI;

        public OrganizationService(IConfiguration configuration, IClientService clientService)
        {
            _organizationsAPI = configuration.GetSection("Organizations.API:Url").Value;
            _clientService = clientService;
        }

        public async Task<GetResponse> Get(Guid id)
        {
            var tokenResponse = await _clientService.RequestClientCredentialsToken();

            using HttpClient client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);

            var result = await client.GetAsync($"{_organizationsAPI}organizations/{id}");
            return await result.GetResultAsync<GetResponse>();
        }
    }
}
