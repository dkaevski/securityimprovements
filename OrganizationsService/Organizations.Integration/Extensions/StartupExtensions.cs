﻿using Microsoft.Extensions.DependencyInjection;
using Organizations.Integration.Services.OrganizationService;

namespace Organizations.Integration.Extensions
{
    public static class StartupExtensions
    {
        public static void AddOrganizationIntegrationServiceCollection(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IOrganizationService, OrganizationService>();
        }
    }
}
