﻿using Common.Domain.Interfaces;
using Users.Domain.Models;

namespace Users.Domain.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    { }
}
