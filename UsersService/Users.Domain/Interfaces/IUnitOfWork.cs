﻿using Common.Domain.Interfaces;

namespace Users.Domain.Interfaces
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        IUserRepository Users { get; set; }
    }
}
