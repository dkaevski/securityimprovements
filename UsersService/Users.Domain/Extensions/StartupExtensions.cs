﻿using AutoMapper;
using Common.Domain.MediatR;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Users.Domain.Mappings;

namespace Users.Domain.Extensions
{
    public static class StartupExtensions
    {
        public static void AddDomainServiceCollection(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddMediatRConfiguration();
            serviceCollection.AddAutoMapperConfiguration();
            serviceCollection.AddFluentValidationConfiguration();
        }

        private static void AddMediatRConfiguration(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddMediatR(Assembly.GetExecutingAssembly());

            serviceCollection.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
        }

        public static void AddAutoMapperConfiguration(this IServiceCollection serviceCollection)
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new BusinessMappingProfile());
            });

            mappingConfig.AssertConfigurationIsValid();

            IMapper mapper = mappingConfig.CreateMapper();
            serviceCollection.AddSingleton(mapper);
        }

        private static void AddFluentValidationConfiguration(this IServiceCollection serviceCollection)
        {
            AssemblyScanner
                .FindValidatorsInAssembly(Assembly.GetExecutingAssembly())
                .ForEach(r => serviceCollection.AddScoped(r.InterfaceType, r.ValidatorType));
        }
    }
}
