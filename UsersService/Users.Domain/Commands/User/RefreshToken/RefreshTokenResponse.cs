﻿using Common.Identity.Models;

namespace Users.Domain.Commands.User.RefreshToken
{
    public class RefreshTokenResponse : IdentityResponse
    { }
}
