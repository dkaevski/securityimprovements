﻿using Common.Domain.MediatR;

namespace Users.Domain.Commands.User.RefreshToken
{
    public class RefreshTokenRequest : RequestBase<RefreshTokenResponse>
    {
        public string RefreshToken { get; set; }
    }
}
