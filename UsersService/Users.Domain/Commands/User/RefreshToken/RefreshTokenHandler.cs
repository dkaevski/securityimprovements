﻿using AutoMapper;
using Common.Domain.MediatR;
using Common.Identity.Services.ClientService;
using System.Threading;
using System.Threading.Tasks;

namespace Users.Domain.Commands.User.RefreshToken
{
    public class RefreshTokenHandler : MediatingRequestHandler<RefreshTokenRequest, RefreshTokenResponse>
    {
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public RefreshTokenHandler(IMapper mapper, IClientService clientService)
        {
            _mapper = mapper;
            _clientService = clientService;
        }

        public override async Task<RefreshTokenResponse> Handle(RefreshTokenRequest request, CancellationToken cancellationToken)
        {
            var response = await _clientService.RequestRefreshToken(request.RefreshToken);

            return await Task.FromResult(_mapper.Map<RefreshTokenResponse>(response));
        }
    }
}
