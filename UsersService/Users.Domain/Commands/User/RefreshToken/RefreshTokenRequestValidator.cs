﻿using FluentValidation;

namespace Users.Domain.Commands.User.RefreshToken
{
    public sealed class RefreshTokenRequestValidator : AbstractValidator<RefreshTokenRequest>
    {
        public RefreshTokenRequestValidator()
        {
            RuleFor(r => r.RefreshToken).NotEmpty();
        }
    }
}
