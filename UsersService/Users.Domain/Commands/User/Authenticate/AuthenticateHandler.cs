﻿using AutoMapper;
using Common.Domain.MediatR;
using Common.Identity.Services.ClientService;
using System.Threading;
using System.Threading.Tasks;

namespace Users.Domain.Commands.User.Authenticate
{
    public class AuthenticateHandler : MediatingRequestHandler<AuthenticateRequest, AuthenticateResponse>
    {
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public AuthenticateHandler(IMapper mapper, IClientService clientService)
        {
            _mapper = mapper;
            _clientService = clientService;
        }

        public override async Task<AuthenticateResponse> Handle(AuthenticateRequest request, CancellationToken cancellationToken)
        {
            var response = await _clientService.RequestPasswordToken(request.UserName, request.Password);

            return await Task.FromResult(_mapper.Map<AuthenticateResponse>(response));
        }
    }
}
