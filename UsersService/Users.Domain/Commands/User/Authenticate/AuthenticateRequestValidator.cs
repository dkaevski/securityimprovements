﻿using FluentValidation;

namespace Users.Domain.Commands.User.Authenticate
{
    public sealed class AuthenticateRequestValidator : AbstractValidator<AuthenticateRequest>
    {
        public AuthenticateRequestValidator()
        {
            RuleFor(r => r.UserName).NotEmpty();
            RuleFor(r => r.Password).NotEmpty();
        }
    }
}
