﻿using Common.Identity.Models;

namespace Users.Domain.Commands.User.Authenticate
{
    public class AuthenticateResponse : IdentityResponse
    { }
}
