﻿using Common.Domain.MediatR;

namespace Users.Domain.Commands.User.Authenticate
{
    public class AuthenticateRequest : RequestBase<AuthenticateResponse>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
