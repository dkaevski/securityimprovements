﻿using Common.Domain.Models;
using System;

namespace Users.Domain.Models
{
    public class User : BaseGuidEntity
    {
        public Guid OrganizationId { get; set; }
    }
}