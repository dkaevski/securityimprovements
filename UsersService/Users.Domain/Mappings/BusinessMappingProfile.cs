﻿using AutoMapper;
using IdentityModel.Client;
using Users.Domain.Commands.User.Authenticate;
using Users.Domain.Commands.User.RefreshToken;

namespace Users.Domain.Mappings
{
    public class BusinessMappingProfile : Profile
    {
        public BusinessMappingProfile()
        {
            CreateMap<TokenResponse, AuthenticateResponse>();
            CreateMap<TokenResponse, RefreshTokenResponse>();
        }
    }
}
