﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Users.Domain.Commands.User.Authenticate;
using Users.Domain.Commands.User.RefreshToken;

namespace Users.API.Controllers
{
    [AllowAnonymous]
    public class AuthenticateController : MediatingController
    {
        public AuthenticateController(IMediator mediator) : base(mediator)
        { }

        [HttpPost]
        public async Task<IActionResult> Authenticate(AuthenticateRequest model)
        {
            return await HandleRequestAsync<AuthenticateRequest, AuthenticateResponse>(model);
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh(RefreshTokenRequest model)
        {
            return await HandleRequestAsync<RefreshTokenRequest, RefreshTokenResponse>(model);
        }
    }
}
