﻿using Common.Identity.Constants;
using Common.Identity.Models;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public class ProfileService : IProfileService
    {
        protected UserManager<ApplicationUser> _userManager;

        public ProfileService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var claims = new List<Claim>();

            var user = _userManager.GetUserAsync(context.Subject).Result;
            if (user.OrganizationId.HasValue)
            {
                claims.Add(new Claim(CustomClaimTypes.Organization, user.OrganizationId.ToString()));
            }

            context.IssuedClaims.AddRange(claims);

            return Task.FromResult(0);
        }
        public Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;
            return Task.FromResult(0);
        }
    }
}
