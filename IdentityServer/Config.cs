﻿using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace IdentityServer
{
    public class Config
    {
        // scopes define the API resources in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("demo.users.api", "DEMO Users API", new string[] {}),
                new ApiResource("demo.organizations.api", "DEMO Organizations API", new string[] { }),
                new ApiResource("demo.orders.api", "DEMO Orders API", new string[] { }),
            };
        }

        // clients want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            // client credentials client
            return new List<Client>
            {
                // demo client
                new Client
                {
                    ClientId = "demo.orders.client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret(configuration.GetValue<string>("ClientSecret").Sha256())
                    },
                    AllowedScopes = { "demo.organizations.api" }
                },

                // resource owner password grant client
                new Client
                {
                    ClientId = "demo.rop.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret(configuration.GetValue<string>("ClientSecret").Sha256())
                    },

                    AllowedScopes = { "demo.orders.api" },
                    AllowOfflineAccess = true,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    AccessTokenLifetime = 3600,
                    IdentityTokenLifetime = 3600
                }
            };
        }
    }
}
